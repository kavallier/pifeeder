# PiFeeder
## An IoT server dog feeder
### Raspberry Pi Zero ARM11 - Rasbian Jessie-lite

Based on this project: http://drstrangelove.net/2013/12/raspberry-pi-power-cat-feeder-updates/

We all need to feed our pets while we're out. Wouldn't it be great if we just had a machine to do that for us? Well now I can!

### Installation

#### Packages Required

Get all the necessary package requirements to run this project.

```
sudo apt-get -y install nginx python3 python-pip python3-pip python-virtualenv python-dev python3-dev sqlite3
```

#### Setup

Clone the project into the space where the app will be used. I create /var/sites/ and place it there for fun.

```
mkdir -p /var/sites
git clone https://kavallier@bitbucket.org/kavallier/pifeeder.git
sudo ./setup.sh
```

#### SSL

Setup SSL by running the following command:

```
sudo ./setup.sh --ssl-only
```

For more information, use the --help flag.