#!flask/bin/python3

from getpass import getpass
import sys

from flask import current_app
from pifeeder import app, bcrypt, db
from pifeeder.models import User

def main():
    with app.app_context():
        db.metadata.create_all(db.engine)

        print("Enter Email:")
        email = input()
        password = getpass()
        assert password == getpass("Password (again):")

        u = User()
        u.email = email
        u.password = bcrypt.generate_password_hash(password).decode("utf-8")
        db.session.add(u)
        db.session.commit()
        print("User added")

if __name__ == "__main__":
    sys.exit(main())