from flask import Flask
from .models import db
from .views import pifeeder, login_manager
from .utils import bcrypt
from werkzeug.contrib.fixers import ProxyFix

import os.path
import sys

# Function provided by Simon Sapin, considered public domain
# flask.pocoo.org/snippets/104/
def build_secret(app, filename="secret"):
    base_dir = os.path.abspath(os.path.dirname(__file__))
    filename = os.path.join(base_dir, filename)

    try:
        app.config['SECRET_KEY'] = open(filename, 'rb').read()
    except IOError:
        print("ERROR: No secret key. Create it with:")
        if not os.path.isdir(os.path.dirname(filename)):
            print("mkdir -p %s", os.path.dirname(filename))
        print('head -c 24 /dev/urandom > %s', filename)
        sys.exit(1)

app = Flask(__name__, static_folder='pifeeder/static', static_url_path='')
app.config.from_pyfile('config.py')

build_secret(app)

db.init_app(app)
login_manager.login_view = "pifeeder.login"
login_manager.init_app(app)
bcrypt.init_app(app)

app.wsgi_app = ProxyFix(app.wsgi_app)
app.register_blueprint(pifeeder)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
