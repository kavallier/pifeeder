import os
base_dir = os.path.abspath(os.path.dirname(__file__))

WTF_CSRF_ENABLED = False

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(base_dir, 'feeder.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(base_dir, 'feeder_db')
SQLALCHEMY_TRACK_MODIFICATIONS = False

DEBUG = True
VERSION = '0.1.8'

GPIOPIN_LEFT = None
GPIOPIN_RIGHT = 12
FREQ = 50
ROT_C = .75
ROT_CC = 12
ROT_TIME = 1.0
