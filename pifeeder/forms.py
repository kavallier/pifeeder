from flask_wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import DataRequired
from .models import User
from .utils import bcrypt

class LoginForm(Form):
    email = TextField('email', validators=[DataRequired()])
    password = TextField('password', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def printErrors(self):
        for field, errors in self.errors.items():
            for error in errors:
                print("Error in the %s field - %s" % (getattr(self, field).label.text, error))

    def validate(self):
        rv = Form.validate(self)

        self.printErrors()

        if not rv:
            return False

        user = User.query.filter_by(email=self.email.data).first()
        if user is None:
            print("BAD EMAIL")
            self.email.errors.append("Unknown email")
            return False

        if not bcrypt.check_password_hash(user.password, self.password.data):
            print("BAD PASS")
            self.password.errors.append("Invalid password")
            return False

        self.user = user
        return True