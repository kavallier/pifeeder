from flask_sqlalchemy import SQLAlchemy
from flask import current_app
from flask_login import current_user
import RPi.GPIO as gpio
import datetime
import time
from enum import Enum

db = SQLAlchemy()

class Direction(Enum):
    left = 1
    right = 2

class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, index=True, unique=True)
    password = db.Column(db.String)
    authenticated = db.Column(db.Boolean, default=False)

    def is_active(self):
        return True

    def get_id(self):
        return chr(self.id)

    def is_authenticated(self):
        return self.authenticated

    def is_anonymous(self):
        return False

    @property
    def serialize(self):
        return {
            'id' : self.id,
            'email' : self.email
        }

class Action(db.Model):
    __tablename__ = 'actions'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User')
    action = db.Column(db.String(64))
    created = db.Column(db.DateTime, default=datetime.datetime.now)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'user' : self.user.serialize,
            'action' : self.action,
            'created' : [self.created.strftime("%Y-%m-%d"), self.created.strftime("%H:%M:%S")]
        }

    @staticmethod
    def save(message):
        a = Action()
        a.user_id = current_user.id
        a.action = message

        db.session.add(a)
        db.session.commit()

    @staticmethod
    def clearTable():
        Action.query.filter(Action.created < datetime.today().date()).delete()
        db.session.commit()

class PiFeeder:
    def __init__(self, isTest=None):
        self.testing = False
        if isTest:
            self.testing = True

        self.pins = {
            Direction.left.name : current_app.config['GPIOPIN_LEFT'],
            Direction.right.name : current_app.config['GPIOPIN_RIGHT'],
        }

        self.frequencyHz = current_app.config['FREQ']
        self.rotation = current_app.config['ROT_C']
        self.rotationCC = current_app.config['ROT_CC']
        self.rotationTime = current_app.config['ROT_TIME']

    def _truncateFloat(self, val, n):
        s = '{}'.format(val)
        i, p, d = s.partition('.')
        return float('.'.join([i, (d+'0'*n)[:n]]))

    def turnFeeder(self, direction, sleepTime):
        activePin = None
        try:
            sleepTime = self._truncateFloat(sleepTime,1)

            if direction in Direction.__members__:
                activePin = self.pins[direction]
            else:
                raise Exception("Incorrect direction")

            self.setup(activePin)
            self.rotate(activePin, sleepTime)
        except Exception as e:
            current_app.logger.error(str(e))
            Action.save("Error!")
            return False

        msg = direction.title() + " feeder used. " + str(sleepTime) + " sec"
        if self.testing is True:
            msg = "TEST: " + msg

        Action.save(msg)
        gpio.cleanup()
        return True

    def setup(self, pin):
        gpio.setmode(gpio.BOARD)
        gpio.setup(pin, gpio.OUT)

    def rotate(self, pin, sleepTime):
        try:
            DCP = self.rotation * 100 / (1000 / self.frequencyHz)
            pwm = gpio.PWM(pin, self.frequencyHz)
            # 10 Hz = 100ms pulse length
            # 50 Hz = 20ms pulse length
            # 100 Hz = 10ms pulse length
            # Period = 1 / Frequency
            # DutyCycle = PulseWidth / Period == PulseWidth * Frequency
            # DC = 0.001s * 50 Hz == 5%
            # Change the duty cycle to 5 percent of signal strength
            if self.testing is False:
                pwm.start(DCP)
                time.sleep(sleepTime)
                pwm.stop()
        except Exception as e:
            current_app.logger.error(str(e))
            gpio.cleanup()
            raise e

from .forms import *
