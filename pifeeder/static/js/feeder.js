(function($){
	$.PiFeeder = {
		init : function(){
			if($('#dispense_left')){
				var f = function(){
					var e = $(this);
					var d = {
						button: e.data('dir'),
						test: $('#testing').is(':checked'),
						time: $('#time').val()
					};

					if($('#time').val()){
						d['time'] = $('#time').val();
					}

					$.ajax({
						method : "POST",
						contentType: "application/json; charset=utf-8",
						url : $.PiFeeder.getUrl('feed'),
						dataType: "json",
						data : JSON.stringify(d),
						error : function(r){
							$.PiFeeder.doMessage(r, e, {
								message : "<strong>ATTN: </strong>Could not deliver food to pupper. Something went wrong. :(",
								css : "alert-warning"
							});
						},
						success : function(r){
							$.PiFeeder.doMessage(r, e, {
								message : "<strong>ATTN: </strong>Yes! Food delivered",
								css : "alert-success",
								options : {
									appendTime : true
								}
							});
						}
					});
				};

				$('#dispense_left').click(f);
				$('#dispense_right').click(f);
			}

			// Do searchable setup
		},
		getUrl : function(key){
			if (key in this.urls)
				return this.urls[key];
			return false;
		},
		getActions : function(){
			var f = function(r, e){
				e.find('tbody').empty();
				for (var i = 0; i < r.length; i++) {
					e.last().append(
						$("<tr></tr>").append(
							$("<td></td>").append(r[i].user.email)
						).append(
							$("<td></td>").append(r[i].action)
						).append(
							$("<td></td>").append(r[i].created[0] + " " + r[i].created[1])
						));
				};
				e.parent().removeClass('hide');
			};

			// this.sendAjaxRequest({
			// 	url: 'actions',
			// 	element: $(".table-bordered"),
			// 	success: { func: f }
			// });

			$.ajax({
				method : "GET",
				contentType: "application/json; charset=utf-8",
				url : $.PiFeeder.getUrl('actions'),
				dataType: "json",
				success : function(r){
					f(r, $(".table-bordered"));
				}
			});
		},
		sendAjaxRequest : function(args){
			var o = {
				method : "POST",
				contentType: "application/json; charset=utf-8",
				url : $.PiFeeder.getUrl(args.url),
				dataType: "json"
			};

			if("data" in args){
				o['data'] = JSON.stringify(args.data);
			}
			if("error" in args){
				o['error'] = function(response){ args.error.func(response, args.element, args.error.data); };
			}
			if("success" in args){
				o['success'] = function(response){ args.success.func(response, args.element, args.success.data); };
			}

			$.ajax(o);
		},
		doMessage : function(response, element, data){
			if (response.test){
				data.message = "TEST: " + data.message;
			}

			if(data.options){
				if(data.options.appendTime)
					data.message = data.message + " at " + (new Date().toLocaleTimeString());
			}

			var p = element.parent();
			p.find('.alert').remove();
			p.prepend($('<div/>',
				{
					'class' : 'alert ' + data.css,
					html: data.message
				}).hide(0).show()
			);

			$.PiFeeder.getActions();
		}
	};

	$(document).ready(function(){
		$.PiFeeder.init();
	});
})(jQuery);
