import json
from datetime import datetime
from dateutil.parser import parse as dateParse
from .cross import crossdomain
from voluptuous import (Schema, Required, All, Length,
                    Range, MultipleInvalid)

from flask import (Blueprint, request, render_template,
                    current_app, redirect, url_for, jsonify)
from flask_login import (LoginManager, login_required, login_user,
                    logout_user, current_user)

from .models import Action, PiFeeder, User, db
from .forms import LoginForm

pifeeder = Blueprint('pifeeder', __name__)
login_manager = LoginManager()

@login_manager.user_loader
def user_loader(user_id):
    return User.query.get(ord(user_id))

@pifeeder.route("/")
@crossdomain('*')
def home():
    return redirect(url_for("pifeeder.feeder"))

@pifeeder.route("/login", methods=["GET", "POST"])
@crossdomain('*')
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = form.user
        if user:
            next = request.args.get('next')
            user.authenticated = True
            db.session.add(user)
            db.session.commit()
            login_user(user, remember=True)
            return redirect(next or url_for(".feeder"))
    return render_template("login.html", form=form)

@pifeeder.route("/logout")
@crossdomain('*')
@login_required
def logout():
    user = current_user
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    return redirect(url_for("pifeeder.login"))

# General rendering
@pifeeder.route('/feeder')
@crossdomain('*')
@login_required
def feeder():
    f = PiFeeder()
    FEED_URL = request.base_url + '/feed'
    ACTION_URL = request.base_url + '/actions'
    return render_template('feedercontent.html', config=current_app.config, feeder=f, FEED_URL=FEED_URL, ACTION_URL=ACTION_URL)

# Handle AJAX request
@pifeeder.route('/feeder/feed', methods=['POST'])
@crossdomain('*')
@login_required
def feed():
    test = True if request.json['test'] in [True, 'true', 'True', '1'] else False
    time = current_app.config['ROT_TIME'] if (request.json['time'] is None or not request.json['time']) else float(request.json['time'])

    schema = Schema({
        Required('button'): All(str, Length(max=5)),
        Required('time', default=3): All(float, Range(min=1, max=5))
    })

    try:
        schema({
            'button': request.json['button'],
            'time': time
        })
    except MultipleInvalid as e:
        resp = jsonify({'status':False,'message': str(e)})
        resp.status_code = 500
        return resp

    f = PiFeeder(test)
    data = { 'status' : f.turnFeeder(request.json['button'], time), 'test' : test }
    data['test2'] = request.json['test']
    resp = jsonify(data)
    if data['status']:
        resp.status_code = 200
    else:
        resp.status_code = 500

    return resp

@pifeeder.route('/feeder/actions', methods=['GET'])
@crossdomain('*')
def actions():
    start = request.args.get('start')
    end = request.args.get('end')

    if start is not None:
        start = dateParse(start)
    if end is not None:
        end = dateParse(end)

    actions = Action.query
    if start is not None and end is not None:
        actions = actions.filter(Action.created >= start.date()).filter(Action.created <= end.date())
    elif start is not None:
        actions = actions.filter(Action.created >= start.date())
    elif end is not None:
        actions = actions.filter(Action.created <= end.date())
    else:
        actions = actions.filter(Action.created >= datetime.today().date())
    actions = actions.order_by(Action.created).all()

    return jsonify([i.serialize for i in actions])

@pifeeder.after_request
def addHeader(response):
    if current_app.config['DEBUG']:
        response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
        response.headers['Cache-Control'] = 'public, max-age=0'

    return response
