#!flask/bin/python3
from pifeeder import app, db

if __name__ == '__main__':
    with app.app_context():
        db.metadata.create_all(bind=db.engine)
    app.run()