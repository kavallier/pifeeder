#!/bin/bash
# Flask PiFeeder Setup script

if [ "$USER" != "root" ]; then
    echo "Run as root."
    exit 0
fi

runner="$(who am i | awk '{print $1}')"
ssl=0

generateSSL(){
    echo "This can take a long time. Are you sure you want to do so? [y/n]"

    if [ $ssl -eq 0 ]; then
        response='0'
        while [[ ! $response =~ ^[y|n]$ ]]
        do
            read response
        done
        [ $response = 'n' ] && return 0
    fi

    pa=/etc/nginx/ssl
    [ ! -d $pa ] && mkdir $pa

    if [ ! -f $pa/nginx.crt ]; then
        openssl req -x509 -nodes -sha256 -days 365 -newkey rsa:2048 -keyout $pa/nginx.key -out $pa/nginx.crt
        openssl dhparam -dsaparam -out $pa/dhparam.pem 4096

        pa=/etc/nginx/sites-available
        cp -f conf/pifeeder_ssl $pa/pifeeder
    fi
}

showHelp(){
    echo "Usage: ./setup.sh [OPTIONS]

Options:
--ssl-only:     Only generate SSL certificate
  -s|--ssl:     Generate SSL certification within the rest of setup
 -h|--help:     Show this message!
"
}

while [[ $# -gt 1 ]]
do
    key="$1"

    case $key in
        --ssl-only)
        generateSSL
        exit 0
        shift
        ;;
        -s|--ssl)
        ssl=1
        shift
        ;;
        -h|--help)
        showHelp
        shift
        ;;
    esac

apt-get update && apt-get -y upgrade
apt-get -y install gcc build-essential nginx python3 python-pip python-virtualenv python3-dev sqlite3 libffi-dev upstart
if [ $(cat /etc/*-release | grep wheezy ) ]; then
    apt-get -y install python-dev
else
    apt-get -y install -t jessie python-dev
fi

# (flask)

if [ ! -d flask ]; then
    python3 -m venv flask --without-pip

    source flask/bin/activate
    curl https://bootstrap.pypa.io/get-pip.py | python3
    deactivate
fi

source flask/bin/activate
pip3 install -r requirements.txt
python3 db_create.py

echo "[INFO]  Create users via addUser.py..."

deactivate
# (exit flask)

# Setup SSL
if [ $ssl -eq 1]; then
    generateSSL
fi

pa=/etc/systemd/system
cp -f conf/pifeeder.conf $pa/pifeeder.service
chmod 644 $pa/pifeeder.conf
chown root:root $pa/pifeeder.conf

pa=/etc/nginx/sites-available
rm /etc/nginx/sites-enabled/default
cp -f conf/pifeeder $pa
chmod 644 $pa/pifeeder
chown root:root $pa/pifeeder
ln -s $pa/pifeeder /etc/nginx/sites-enabled

chown -R "$(stat -c '%U' $(pwd))":www-data $(pwd)

al="/usr/bin/sudo -H stop pifeeder; /usr/bin/sudo -H start pifeeder"
alias feeder-restart=$al
echo $al >> /home/$runner/.bash_aliases

exit 0
